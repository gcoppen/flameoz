(function ($) {
  Drupal.behaviors.media_slideshow = {
    attach: function (context) {
      $('.media-youtube-media_slideshow').once('media_slideshow').load(function() {
        iframe_id = $(this).data('iframe_id');
        video_id = $(this).data('video_id');
        views_id = $(this).data('views_id');

        // Example of interacting with YouTube API's
        //@link http://jsfiddle.net/masiha/4mEDR/
        var media_slideshow_player = new YT.Player(iframe_id, {
          videoId: video_id,
          events: {
            'onStateChange': function (event) {
              if (event.data == YT.PlayerState.PLAYING) {
                Drupal.viewsSlideshow.action({
                  "action": 'pause',
                  "slideshowID": views_id,
                  "force": true
                });
              }
              else if (event.data == YT.PlayerState.PAUSED)  {
                Drupal.viewsSlideshow.action({
                  "action": 'play',
                  "slideshowID": views_id,
                  "force": true
                });
              }
            }
          }
        });
      });
    }
  };
})(jQuery);
