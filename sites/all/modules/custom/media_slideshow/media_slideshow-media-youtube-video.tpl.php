<div class="<?php print $classes; ?>  media-youtube-<?php print $id; ?>">
  <iframe id="<?php print $iframe_id; ?>"
          class="media-youtube-player media-youtube-media_slideshow" <?php print $api_id_attribute; ?>
          width="<?php print $width; ?>"
          height="<?php print $height; ?>"
          title="<?php print $title; ?>"
          src="<?php print $url; ?>"
          frameborder="0"
          allowfullscreen
          data-iframe_id='<?php print $iframe_id; ?>'
          data-video_id='<?php print $video_id; ?>'
          data-views_id='<?php print $views_id; ?>'
    >
    <?php print $alternative_content; ?></iframe>
</div>
