/**
 * @file Display a load of square thumbnail images as a jCarousel
 */


//jQuery(document).ready(function() {
//  var options = { 
//    btnNext:  "#flameozmod-carousel-wrapper .carousel-next",
//    btnPrev:  "#flameozmod-carousel-wrapper .carousel-prev",
//    visible:  4,
//    scroll:   4,
//    auto:     4000,
//    speed:    2000,
//    vertical: false,
//    circular: true,
//    start:    0
//  };
//  jQuery("#flameozmod-carousel").jCarouselLite(options);
//});



(function ($) {
  Drupal.behaviors.flameozmod_carousel = {
    attach: function (context, settings) {
      console.log('flameozmod_carousel');
      var options = { 
        btnNext:  "#flameozmod-carousel-wrapper .carousel-next",
        btnPrev:  "#flameozmod-carousel-wrapper .carousel-prev",
        visible:  4,
        scroll:   4,
        auto:     4000,
        speed:    2000,
        vertical: false,
        circular: true,
        start:    0
      };
      $("#flameozmod-carousel").jCarouselLite(options);
    }
  };
}(jQuery));
