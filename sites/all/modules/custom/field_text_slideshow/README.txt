

--- README  -------------------------------------------------------------

Field Slideshow

Provides a Slideshow format for displaying Image and Media fields, using the JQuery Cycle plugin.

Compared to Views slideshows, building the slideshow from multiple nodes, this module builds it from a single node, if you're using a multi-valued Image field.


--- INSTALLATION & USAGE-------------------------------------------------

1 - Extract the archive into sites/all/modules
2 - Go to "Content types" (/admin/structure/types)
3 - Select "Manage display" for a content type that contains a multi-valued Image field
4 - In the Format column, under "Format", you can now choose "Slideshow"
5 - Click the settings cog to display the available options
6 - Save! and here you go.


--- AVAILABLE OPTIONS ---------------------------------------------------

Image style
Caption
Link
Transition effect
Transition speed
Timeout
Pager (numbers or thumbnails)
Prev/Next controls
Pause on hover

Integration with Colorbox.



Written by Jerome Danthinne
http://www.grincheux.be
