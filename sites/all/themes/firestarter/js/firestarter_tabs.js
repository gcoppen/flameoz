/*
 * 
 * Render content into tabs
 * 
 */
//http://jqueryfordesigners.com/jquery-tabs/
jQuery(document).ready(function(){
  //alert('m');
  var tabContainers = jQuery('div.fs-tabs > div');
  tabContainers.hide().filter(':first').show();
  
  jQuery('div.fs-tabs ul.fs-tabs-nav a').filter(':first').addClass('selected');
  
  jQuery('div.fs-tabs ul.fs-tabs-nav a').click(function () {
    tabContainers.hide();
    tabContainers.filter(this.hash).show();
    jQuery('div.fs-tabs ul.fs-tabs-nav a').removeClass('selected');
    jQuery(this).addClass('selected');
    return false;
  }).filter(':first').click();
  
  // multiple tab blocks
//  tabContainers.each(function(index) {
    //alert(index + ': ' + jQuery(this).text());
//    jQuery(this).hide().filter(':first').show();
//    
//    
//    jQuery(this + ' ul.fs-tabs-nav a').filter(':first').addClass('selected');
//  
//    jQuery(this + ' ul.fs-tabs-nav a').click(function () {
//      tabContainers.hide();
//      tabContainers.filter(this.hash).show();
//      //on all?
//      jQuery('div.fs-tabs ul.fs-tabs-nav a').removeClass('selected');
//      jQuery(this).addClass('selected');
//      return false;
//    }).filter(':first').click();
    
//  });
});