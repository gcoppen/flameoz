/*
 * This jQuery code controls the rollover states on the homepages
 * 
 * overlay graphics
 * #main-overlay-0
 * #main-overlay-1
 * #main-overlay-2
 * #main-overlay-3
 *
 * column links
 * #main-triple-1
 * #main-triple-2
 * #main-triple-3
 */


jQuery(document).ready(function(){
  jQuery("#main-overlay-1").css("display", "none");
  jQuery("#main-overlay-2").css("display", "none");
  jQuery("#main-overlay-3").css("display", "none");


  jQuery("#main-triple-1").hover(function(){
    jQuery("#main-overlay-1").fadeIn("slow");
    jQuery("#main-overlay-2").fadeOut("slow");
    jQuery("#main-overlay-3").fadeOut("slow");
  });

  jQuery("#main-triple-2").hover(function(){
    jQuery("#main-overlay-2").fadeIn("slow");
    jQuery("#main-overlay-1").fadeOut("slow");
    jQuery("#main-overlay-3").fadeOut("slow");
  });

  jQuery("#main-triple-3").hover(function(){
    jQuery("#main-overlay-3").fadeIn("slow");
    jQuery("#main-overlay-1").fadeOut("slow");
    jQuery("#main-overlay-2").fadeOut("slow");
  });

});
