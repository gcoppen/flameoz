/*
 * This jQuery code opens/closes things
 */

jQuery(document).ready(function(){
 
  //.open-this
  jQuery(".open-this").css("display", "none");

  jQuery(".click-open").click(function(){
    jQuery(this).next(".open-this").slideToggle();
  });

  // Read More field
  // Read More: should be Read More + or Read Less -
  
  var read_more = '<span>Read More +</span>';
      read_more += '<span style="display:none;">Read Less -</span>';
  jQuery(".field-name-field-body-extended .field-label").html(read_more);
  
  jQuery(".field-name-field-body-extended .field-items").css("display", "none");

  jQuery(".field-name-field-body-extended .field-label").click(function(){
    jQuery(this).next(".field-items").slideToggle();
    jQuery(".field-name-field-body-extended .field-label span").toggle();
  });
});
